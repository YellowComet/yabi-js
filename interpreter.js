// SPDX-License-Identifier: GPL-2.0-or-later
/*
Copyright 2023 Alex <mailto:alex@blueselene.com>

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.

*/

class YabiJS {
	constructor(source, input, output) {
		this.input = input;
		this.inputPointer = 0;
		this.output = output;
		this.brainfuckProgram = source;
		this.tokenizedProgram = [];
		this.bracketInformation = [];
		this.dataPointer = 0;
		this.programCounter = 0;
		this.memory = new Int8Array(30000).fill(0);

		// Opcodes
		this.DP_INC = 1; // ">"
		this.DP_DEC = 2; // "<"
		this.INC = 3; // "+"
		this.DEC = 4; // "-"
		this.OUT = 5; // "."
		this.IN = 6; // ","
		this.LEFT_BR = 7 // "["
		this.RIGHT_BR = 8 // "]"
	}
	parse() {
		// This function parses the initial brainfuck program into tokens, skipping invalid characters
		for (let i = 0; i < this.brainfuckProgram.length; i++) {
			switch(this.brainfuckProgram[i]) {
				case '>':
					this.tokenizedProgram.push({opcode: this.DP_INC, value: 1});
					break;
				case '<':
					this.tokenizedProgram.push({opcode: this.DP_DEC, value: 1});
					break;
				case '+':
					this.tokenizedProgram.push({opcode: this.INC, value: 1});
					break;
				case '-':
					this.tokenizedProgram.push({opcode: this.DEC, value: 1});
					break;
				case '.':
					this.tokenizedProgram.push({opcode: this.OUT, value: 1});
					break;
				case ',':
					this.tokenizedProgram.push({opcode: this.IN, value: 1});
					break;
				case '[':
					this.tokenizedProgram.push({opcode: this.LEFT_BR, value: 1});
					break;
				case ']':
					this.tokenizedProgram.push({opcode: this.RIGHT_BR, value: 1});
					break;
			}
		}
		this.analyzeBrackets();
		return;
	}
	analyzeBrackets() {
		// Not for external use! This is run automatically
		// Finds pairs of "["/"]", returns an array of objects consisting of the location of each "[" in the program and the location of their matching "]"
		// Doesn't work with the raw program! Program must have been broken up into tokens by YabiJS.parse()

		var nesting = [];
		for (let i = 0; i < this.tokenizedProgram.length; i++) {
			switch(this.tokenizedProgram[i].opcode) {
				case this.LEFT_BR:
					nesting.push(i);
					break;
				case this.RIGHT_BR:
					if (nesting.length < 1) {
						console.error('yabi-js: Right bracket without matching left bracket!');
					} else {
						// .left = position of left bracket, .right = position of right bracket
						this.bracketInformation.push({left: nesting.pop(), right: i});
					}
					break;
			}
		}
		if (nesting.length > 0) {
			console.error('yabi-js: Left bracket without matching right bracket!');
		}
		return;
	}
	step() {
		// This is where the actual execution takes place, one opcode is executed per call
		// If running an optimized program, since multiple consecutive calls of >, <, +, or - are packed into a single token, they will be executed all at once!
		// See Codeberg.org/YellowComet/yabi-js for more about the optimizations

		// This function returns true if the program has finished, false otherwise
		if (this.tokenizedProgram.length === this.programCounter) {
			return true; // Execution finished
		}
		switch (this.tokenizedProgram[this.programCounter].opcode) {
			case this.DP_INC:
				this.dataPointer = this.dataPointer + this.tokenizedProgram[this.programCounter].value;
				this.programCounter++;
				break;
			case this.DP_DEC:
				this.dataPointer = this.dataPointer - this.tokenizedProgram[this.programCounter].value;
				this.programCounter++;
				break;
			case this.INC:
				this.memory[this.dataPointer] = this.memory[this.dataPointer] + this.tokenizedProgram[this.programCounter].value;
				this.programCounter++;
				break;
			case this.DEC:
				this.memory[this.dataPointer] = this.memory[this.dataPointer] - this.tokenizedProgram[this.programCounter].value;
				this.programCounter++;
				break;
			case this.IN:
				if (this.input.length < this.inputPointer) {
					this.memory[this.dataPointer] = -1; //EOF
				} else {
					this.memory[this.dataPointer] = this.input.charCodeAt(this.inputPointer);
					this.inputPointer++;
				}
				this.programCounter++;
				break;
			case this.OUT:
				this.output.innerText += String.fromCharCode(this.memory[this.dataPointer]);
				this.programCounter++;
				break;
			case this.LEFT_BR:
				if (this.memory[this.dataPointer] === 0) {
					for (let i = 0; i < this.bracketInformation.length; i++) {
						if (this.bracketInformation[i].left === this.programCounter) {
							this.programCounter = this.bracketInformation[i].right + 1;
							return false;
						}
					}
				} else {
					this.programCounter++;
				}
				break;
			case this.RIGHT_BR:
				if (this.memory[this.dataPointer] !== 0) {
					for (let i = 0; i < this.bracketInformation.length; i++) {
						if (this.bracketInformation[i].right === this.programCounter) {
							this.programCounter = this.bracketInformation[i].left + 1;
							return false;
						}
					}
				} else {
					this.programCounter++;
				}
				break;
			default:
				console.error('Invalid opcode ' + this.tokenizedProgram[this.programCounter].opcode + ' at ' + this.programCounter);
				return true;
				break;
		}
	return false;
	}
}

