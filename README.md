# yabi-js

Yet Another Brainfuck Interpreter

## Brainfuck?

Brainfuck is an esoteric programming language. Check out [Wikipedia](https://en.wikipedia.org/wiki/Brainfuck) to learn more about it.

## How to run

Include interpreter.js on the head tag.

Then, just give the interpreter a brainfuck program, a string to use as input, and a DOM element to use as output (DOM element must have .innerText property, almost any element will do), and spam `step()`!

Once the program has finished running, the `step` function will return `true`. If it hasn't finished yet, it will return `false`.

For example:

```javascript

let interp = new YabiJS('++++++++[>++++[>++>+++>+++>+<<<<-]>+>+>->>+[<]<-]>>.>---.+++++++..+++.>>.<-.<.+++.------.--------.>>+.>++.', document.getElementById('input').innerText, document.getElementById('output'));
interp.parse(); // MUST be run!
for (let i = 0; i < 100000; i++) {
	let check = interp.step();
	if (check) {
		break;
	}
}

```

## Compatibility

This interpreter implements memory as an array of signed 8-bit integers, checkout [Int8Array at MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Int8Array).

It also implements EOF as a -1, meaning, if you run out of input, `,` will return -1.

## What's so unique about yabi?

Well, not much really. Perhaps the only thing setting it apart from just any interpreter is that it has the ability to optimize the program by grouping together consecutive `+`, `-`, `<`, `>`, and that it records the positions of every pair of brackets prior to execution, instead of looking for them furing execution. Grouping together consecutive instructions isn't implemented yet, tho the "infrastructure" is there in the code. It will be implemented SoonTM.

Checkout the wiki to learn all about the misteries of the interpreter.

## License

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
